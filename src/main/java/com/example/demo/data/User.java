package com.example.demo.data;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Data
@Entity
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String firstName;
    private String lastName;
    private String gender;
    private String interests;

    public User(){}

    public User(String firstName, String lastName, String gender, String interests) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.gender = gender;
        this.interests = interests;
    }
}