package com.example.demo.data.repository;

import com.example.demo.data.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface UserRepository extends JpaRepository<User, Long> {


    List<User> findByFirstNameAndLastName(String firstName, String lastName);

    List<User> findByFirstName(String fistName);

    @Query("SELECT user FROM User user WHERE user.firstName = ?1")
    List<User> findByFirstNameQuery(String firstName);


    @Query("SELECT user FROM User user WHERE user.firstName = :firstName")
    List<User> findByFirstNameQuery2(@Param(value = "firstName") String firstName);
}
