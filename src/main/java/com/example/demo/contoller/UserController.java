package com.example.demo.contoller;

import com.example.demo.data.User;
import com.example.demo.services.UserService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class UserController {

    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }


    @RequestMapping(value = "/userParams", method = RequestMethod.GET)
    public List<User> findByParams(@RequestParam(value = "firstName") String firstName,
                                   @RequestParam(value = "lastName") String lastName){
        return userService.findByParams(firstName, lastName);
    }

    @RequestMapping(value = "/users", method = RequestMethod.POST)
    public void addUser(@RequestBody User user) {
        userService.addUser(user);
    }

    @RequestMapping(value = "/users", method = RequestMethod.GET)
    public List<User> findAllUsers() {
        return userService.findAllUsers();
    }

    @RequestMapping(value = "/users/{id}", method = RequestMethod.GET)
    public User findUserById(@PathVariable Long id) {
        return userService.findById(id);
    }

    @RequestMapping(value = "/users/{id}", method = RequestMethod.PUT)
    public void modifyUser(@RequestBody User user, @PathVariable Long id) {
        userService.modifyUser(user, id);
    }


}
