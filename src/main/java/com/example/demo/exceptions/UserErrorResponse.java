package com.example.demo.exceptions;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.time.LocalDate;

@Data
@Builder
@AllArgsConstructor
public class UserErrorResponse {
    String status;
    String message;
    LocalDate localDate;

}
