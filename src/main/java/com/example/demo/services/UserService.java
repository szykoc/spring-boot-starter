package com.example.demo.services;

import com.example.demo.data.User;
import com.example.demo.data.repository.UserRepository;
import com.example.demo.exceptions.UserNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserService {

    private final UserRepository userRepository;

    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public void addUser(User user) {
        userRepository.save(user);
    }


    public List<User> findByParams(String firstName, String lastName) {
        List<User> usersByFirstName = userRepository.findByFirstNameAndLastName(firstName, lastName);
        return usersByFirstName;
    }



    public List<User> findAllUsers() {
        return (List )userRepository.findAll();
    }

    public void modifyUser(User user, Long id) {
        userRepository.findById(id).map(
                userFromDatabase -> {
                    userFromDatabase.setFirstName(user.getFirstName());
                    userFromDatabase.setLastName(user.getLastName());
                    userFromDatabase.setGender(user.getGender());
                    userFromDatabase.setInterests(user.getInterests());
                    return userRepository.save(userFromDatabase);
                }
        ).orElseGet(() -> {
                    user.setId(id);
                    return userRepository.save(user);
                }
        );
    }

    public User findById(Long id) {

        return userRepository.findById(id)
                .orElseThrow(() -> new UserNotFoundException("Nie ma usera"));
/*
        Optional<User> maybeUser = userRepository.findById(id);
        boolean present = maybeUser.isPresent();
        if (present){
            return maybeUser.get();
        }
        throw new UserNotFoundException();*/
    }


}
