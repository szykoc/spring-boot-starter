package com.example.demo.configuration;

import com.example.demo.data.User;
import com.example.demo.data.repository.UserRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@Slf4j
public class UserAppConfiguration {

    @Bean
    CommandLineRunner loadDataBase(UserRepository userRepository){
        return args -> {
            log.info("Loading db" + userRepository.save(new User("Szymek", "Kociuba", "MALE", "SPORT")));
            log.info("Loading db" + userRepository.save(new User("Kasia", "Kowalska", "MALE", "SPORT")));
            log.info("Loading db" + userRepository.save(new User("Marek", "Matras", "MALE", "SPORT")));
            log.info("Loading db" + userRepository.save(new User("Kasia", "Zieba", "MALE", "SPORT")));
            log.info("Loading db" + userRepository.save(new User("Marek", "Kowalska", "MALE", "SPORT")));
        };
    }

}
